#pragma once

#include <common.h>

class Delegate
{
	struct Func
	{
		void (*f)(void *, void *);
		void * param;
		Func(void (*f)(void *, void *), void * param)
		{
			this->f = f;
			this->param = param;
		}
	};
	vector<Func> funcs;
public:

	void add(void (*f)(void *, void *), void * param)
	{
		for (auto i = funcs.begin(); i != funcs.end(); i++)
			if (i->f == f && i->param == param)
				return;
		funcs.push_back(Func(f, param));
	}

	void remove(void (*f)(void *, void *), void * param)
	{
		for (auto i = funcs.begin(); i != funcs.end(); i++)
			if (i->f == f && i->param == param)
			{
				funcs.erase(i);
				return;
			}
	}

	void operator()(void * param)
	{
		for (auto i = funcs.begin(); i != funcs.end(); i++)
			(i->f)(i->param, param);
	}
};