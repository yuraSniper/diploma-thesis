#pragma once

#include <common.h>

class Window;

enum Mouse
{
	LEFT = VK_LBUTTON,
	RIGHT = VK_RBUTTON,
	MIDDLE = VK_MBUTTON,
	X1 = VK_XBUTTON1,
	X2 = VK_XBUTTON2
};

class Input
{
	shared_ptr<Window> wnd;
	bool keyPressedState[256];
	unsigned char lastKey;
	int dw;

	static void onKey(void * param, void * key);
	static void onMouseWheel(void * param, void * d);
public:
	Input(shared_ptr<Window> w);

	bool getKey(unsigned char key);
	bool getKeyPressed(unsigned char key);
	unsigned char getLastKeyDown();
	unsigned char getLastKeyPressed();
	bool getMouseButton(Mouse button);
	bool getMouseButtonPressed(Mouse button);
	
	void setClientCursorPos(int x, int y);
	void setScreenCursorPos(int x, int y);
	POINT getMousePos();
	int getMouseWheelDelta();
};