#include "Window.h"

void Window::setupWindowClass()
{
	WNDCLASSEX wc = {0};
	wc.cbSize = sizeof(wc);
	wc.style = 0;
	wc.hbrBackground = nullptr;//CreateSolidBrush(0);
	wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wc.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
	wc.hInstance = hInst = GetModuleHandle(nullptr);
	wc.lpfnWndProc = (WNDPROC)WndProc;
	wc.lpszClassName = "Test Window";

	RegisterClassEx(&wc);
}

LRESULT Window::WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	int delta, tmp = -1;
	POINT mouse;
	Window & This = *(Window *) GetProp(hWnd, "This");

	mouse.x = GET_X_LPARAM(lParam);
	mouse.y = GET_Y_LPARAM(lParam);

	switch (msg)
	{
		case WM_LBUTTONDOWN:
		case WM_LBUTTONUP:
			tmp = VK_LBUTTON;
			This.onKey(&tmp);
			return 0;

		case WM_RBUTTONDOWN:
		case WM_RBUTTONUP:
			tmp = VK_RBUTTON;
			This.onKey(&tmp);
			return 0;

		case WM_MBUTTONDOWN:
		case WM_MBUTTONUP:
			tmp = VK_MBUTTON;
			This.onKey(&tmp);
			return 0;

		case WM_XBUTTONDOWN:
		case WM_XBUTTONUP:
			tmp = VK_XBUTTON1 + HIWORD(wParam);
			This.onKey(&tmp);
			return 0;

		case WM_KEYDOWN:
		case WM_KEYUP:
			This.onKey(&wParam);
			return 0;

		case WM_ERASEBKGND:
			return 1;

		case WM_MOUSEWHEEL:
			delta = GET_WHEEL_DELTA_WPARAM(wParam);
			This.onMouseWheel(&delta);
			return 0;
			
		case WM_DESTROY:
			This.onCLose(nullptr);
			return 0;
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}

Window::Window(int width, int height)
{
	setupWindowClass();
	RECT rt, rt1;
	GetWindowRect(GetDesktopWindow(), &rt1);

	int style = WS_OVERLAPPEDWINDOW;
	int exStyle = 0;

	rt.left = rt.top = 0;
	rt.right = width;
	rt.bottom = height;
	AdjustWindowRectEx(&rt, style, 0, exStyle);
	width = rt.right - rt.left;
	height = rt.bottom - rt.top;

	hWnd = CreateWindowEx(exStyle, "Test Window", "", style,
		(rt1.right - width) / 2, (rt1.bottom - height) / 2,
		width, height, nullptr, nullptr, hInst, nullptr);
	
	SetProp(hWnd, "This", this);

	hDC = GetDC(hWnd);
	PIXELFORMATDESCRIPTOR pfd = {0};
	pfd.nVersion = 1;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 32;
	pfd.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;

	SetPixelFormat(hDC, ChoosePixelFormat(hDC, &pfd), &pfd);
}

HWND Window::getHWND()
{
	return hWnd;
}

HDC Window::getHDC()
{
	return hDC;
}

void Window::messageLoop()
{
	MSG msg;
	if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

void Window::setVisibility(bool visibility)
{
	ShowWindow(hWnd, visibility? SW_SHOW : SW_HIDE);
}

void Window::setMouseCapture(bool visibility)
{
	ShowCursor(visibility);
}

void Window::setTitle(string title)
{
	SetWindowText(hWnd, title.c_str());
}

bool Window::isVisible()
{
	WINDOWPLACEMENT wp;
	wp.length = sizeof(wp);
	GetWindowPlacement(hWnd, &wp);
	return wp.showCmd != SW_HIDE && wp.showCmd != SW_MINIMIZE;
}

bool Window::isCursorVisible()
{
	CURSORINFO ci;
	ci.cbSize = sizeof(ci);
	GetCursorInfo(&ci);
	return ci.flags != 0;
}

bool Window::isActive()
{
	return GetForegroundWindow() == hWnd;
}

int Window::getClientWidth()
{
	RECT rt;
	GetClientRect(hWnd, &rt);
	return rt.right - rt.left;
}

int Window::getClientHeight()
{
	RECT rt;
	GetClientRect(hWnd, &rt);
	return rt.bottom - rt.top;
}

void Window::swapBuffers()
{
	SwapBuffers(hDC);
}