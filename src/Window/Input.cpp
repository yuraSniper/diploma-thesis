#include "Input.h"
#include "Window.h"

void Input::onKey(void * param, void * key)
{
	unsigned char k = *(int *) key;
	switch (k)
	{
		case VK_SHIFT :
			k = GetAsyncKeyState(VK_LSHIFT) ? VK_LSHIFT : VK_RSHIFT;
			break;
		case VK_CONTROL:
			k = GetAsyncKeyState(VK_LCONTROL) ? VK_LCONTROL : VK_RCONTROL;
			break;
		case VK_MENU:
			k = GetAsyncKeyState(VK_LMENU) ? VK_LMENU : VK_RMENU;
			break;
	}
	if ((GetKeyState(k) >> 7) != 0)
	{
		((Input *) param)->lastKey = k;
		((Input *) param)->keyPressedState[k] = true;
	}
}

void Input::onMouseWheel(void * param, void * d)
{
	((Input *)param)->dw += *(int *)d;
}

Input::Input(shared_ptr<Window> w)
{
	wnd = w;
	wnd->onKey.add(onKey, this);
	wnd->onMouseWheel.add(onMouseWheel, this);

	dw = 0;

	for (short i = 0; i < 256; i++)
	{
		keyPressedState[i] = false;
	}
}

bool Input::getKeyPressed(unsigned char key)
{
	if (GetForegroundWindow() != wnd->getHWND())
		return false;

	bool b = getKey(key);

	if (!b && keyPressedState[key])
	{
		keyPressedState[key] = false;
		return true;
	}

	return false;
}

bool Input::getKey(unsigned char key)
{
	if (GetForegroundWindow() != wnd->getHWND())
		return false;

	bool b = GetAsyncKeyState(key) != 0;
	keyPressedState[key] |= b;

	return b;
}

unsigned char Input::getLastKeyDown()
{
	unsigned char tmp = lastKey;
	lastKey = 0;
	return tmp;
}

unsigned char Input::getLastKeyPressed()
{
	unsigned char tmp = lastKey;

	if (getKey(lastKey))
		return 0;

	lastKey = 0;
	return tmp;
}

int Input::getMouseWheelDelta()
{
	int d = dw;
	dw = 0;
	return d;
}

bool Input::getMouseButton(Mouse button)
{
	return getKey(button);
}

bool Input::getMouseButtonPressed(Mouse button)
{
	return getKeyPressed(button);
}

void Input::setClientCursorPos(int x, int y)
{
	POINT p = {x, y};
	ClientToScreen(wnd->getHWND(), &p);
	setScreenCursorPos(p.x, p.y);
}

void Input::setScreenCursorPos(int x, int y)
{
	SetCursorPos(x, y);
}

POINT Input::getMousePos()
{
	POINT p;
	GetCursorPos(&p);
	ScreenToClient(wnd->getHWND(), &p);
	return p;
}