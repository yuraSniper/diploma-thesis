#pragma once

#include <common.h>
#include <Util\Delegate.h>

class Window
{
	HWND hWnd;
	HINSTANCE hInst;
	HDC hDC;
	int width, height;
	void setupWindowClass();
	static LRESULT WndProc(HWND, UINT, WPARAM, LPARAM);
public:
	Delegate onKey, onMouseWheel;
	Delegate onCLose;

	Window(int width, int height);
	HWND getHWND();
	HDC getHDC();
	void messageLoop();

	void setVisibility(bool visibility);
	void setMouseCapture(bool visibility);
	void setTitle(string title);
	bool isVisible();
	bool isCursorVisible();
	bool isActive();
	int getClientWidth();
	int getClientHeight();
	void swapBuffers();
};