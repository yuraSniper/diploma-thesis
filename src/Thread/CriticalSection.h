#pragma once

#include <common.h>

class CriticalSection
{
	CRITICAL_SECTION crit;
public:

	void init()
	{
		InitializeCriticalSection(&crit);
	}

	void lock()
	{
		EnterCriticalSection(&crit);
	}

	bool tryLock()
	{
		return TryEnterCriticalSection(&crit);
	}

	void unlock()
	{
		LeaveCriticalSection(&crit);
	}
};