#pragma once

#include <Thread\Thread.h>

class GeneralThread : public Thread
{
	bool working = true;

public:
	virtual void run()
	{
		bool queueEmpty;
		while (working)
		{
			if (crit.tryLock())
			{
				queueEmpty = taskQueue.empty();
				if (!queueEmpty)
				{
					(*taskQueue.begin())->operate();
					taskQueue.erase(taskQueue.begin());
				}
				crit.unlock();
			}
			if (queueEmpty)
				Sleep(1);
		}
	}
};