#include "TextRenderer.h"

#include <Render\Resource\ResourceManager.h>

TextRenderer & TextRenderer::getInstance()
{
	static TextRenderer instance;
	return instance;
}

TextRenderer::TextRenderer()
{
	glGenBuffers(1, &vbo);
	glEnableClientState(GL_VERTEX_ARRAY);

	font = ResourceManager::getInstance().loadFont("data\\Textures\\Font.png");

	font.bind(1);
}

void TextRenderer::renderStringInternal(string str)
{
	int prevTex;
	glGetIntegerv(GL_ACTIVE_TEXTURE, &prevTex);

	glActiveTexture(GL_TEXTURE1);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	vector<Glyph> data;

	for (int i = 0; i < str.length(); i++)
	{
		int ch = (unsigned char)(str.at(i)) - 32;
		float s = (ch % 16) / 16.0f + (7 / 512.0f);
		float t = (ch / 16) / 16.0f;
		float sNext = s + 1 / 16.0f - (7 / 512.0f);
		float tNext = t + 1 / 16.0f;

		data.push_back({i, 0, s, t});
		data.push_back({i, 1, s, tNext});
		data.push_back({(i + 1), 1, sNext, tNext});
		data.push_back({(i + 1), 0, sNext, t});
	}

	glBufferData(GL_ARRAY_BUFFER, sizeof(Glyph) * data.size(), data.data(), GL_STREAM_DRAW);

	glVertexPointer(2, GL_FLOAT, sizeof(Glyph), (void *) offsetof(Glyph, x));
	glClientActiveTexture(GL_TEXTURE1);
	glTexCoordPointer(2, GL_FLOAT, sizeof(Glyph), (void *) offsetof(Glyph, s));
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	glPushAttrib(GL_CURRENT_BIT | GL_ENABLE_BIT);
	
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_TEXTURE_2D);
	glColor4f(r, g, b, a);
	glDrawArrays(GL_QUADS, 0, data.size());
	
	glPopAttrib();

	glActiveTexture(prevTex);
}

void TextRenderer::renderStringFormat(float x, float y, string format, ...)
{
	char *str = new char[1024];
	va_list list;


	va_start(list, format);
	vsprintf_s(str, 1024, format.c_str(), list);
	va_end(list);

	glPushMatrix();
	glLoadIdentity();
	glTranslatef(x, y, 0);
	glScalef(width, height, 1);

	char * lastLineEnd = str;
	for (int i = 0; i < 1024 && str[i] != 0; i++)
	{
		if (str[i] == '\n')
		{
			renderStringInternal(string(lastLineEnd, str + i));

			glTranslatef(0, 1, 0);

			lastLineEnd = str + i + 1;
		}
	}

	renderStringInternal(string(lastLineEnd));

	delete str;

	glPopMatrix();
}

void TextRenderer::setColor(float r, float g, float b, float a)
{
	this->r = r;
	this->g = g;
	this->b = b;
	this->a = a;
}