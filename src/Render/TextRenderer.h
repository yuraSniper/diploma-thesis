#pragma once

#include <common.h>
#include <Render\Resource\Font.h>

class ShaderProgram;

class TextRenderer
{
	struct Glyph
	{
		float x, y;
		float s, t;
	};

	unsigned int vbo;
	float r = 1;
	float g = 1;
	float b = 1;
	float a = 1;


	Font font;

	TextRenderer();
	TextRenderer(const TextRenderer &);
	TextRenderer & operator=(TextRenderer &);

	void renderStringInternal(string str);

public:
	float width = 11, height = 13;

	static TextRenderer & getInstance();

	void renderStringFormat(float x, float y, string format, ...);
	void setColor(float r, float g, float b, float a);
};