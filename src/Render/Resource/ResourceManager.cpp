#include "ResourceManager.h"

#include <Render\Resource\TextureAtlas.h>
#include <Render\Resource\Texture.h>

ResourceManager & ResourceManager::getInstance()
{
	static ResourceManager instance;
	return instance;
}

ResourceManager::PNGTex ResourceManager::loadTextureFromFile(string path, int type)
{
	PNGTex tex;
	tex.data = new vector<unsigned char>;

	vector<unsigned char> buffer;

	ifstream file;
	file.open(path, ios::in | ios::binary);
	buffer.assign(istreambuf_iterator<char>(file), istreambuf_iterator<char>());
	file.close();

	lodepng::State state;
	state.info_raw.colortype = (LodePNGColorType) type;

	lodepng::decode(*tex.data, tex.size, tex.size, state, buffer);

	return tex;
}

Texture ResourceManager::storeTextureInFreeAtlas(PNGTex texture)
{
	for (int i = 0; i < textureAtlases.size(); i++)
	{
		TextureAtlas atlas = textureAtlases[i];
		int atlasTexSize = atlas.getTexureSize();
		if (atlas.getFreeSlots() > 0 && atlasTexSize == texture.size)
		{
			int slot = atlas.getMaxSlots() - atlas.getFreeSlots();

			int row = (slot / atlas.getTexturesInRow());
			int column = slot % atlas.getTexturesInRow();

			Texture tex(atlas, i, textures.size(), slot);

			textures.push_back(tex);

			glBindTexture(GL_TEXTURE_2D, atlas.getId());
			glTexSubImage2D(GL_TEXTURE_2D, 0, column * atlasTexSize, row * atlasTexSize, atlasTexSize, atlasTexSize, GL_RGBA, GL_UNSIGNED_BYTE, &((*texture.data)[0]));
			glBindTexture(GL_TEXTURE_2D, 0);

			textureAtlases[i].decrementFreeSlots();

			delete texture.data;

			return tex;
		}
	}
	return Texture(TextureAtlas(-1, -1, -1), -1, 0);
}

Texture ResourceManager::loadTexture(string path)
{
	PNGTex tex = loadTextureFromFile(path);
	Texture texture = storeTextureInFreeAtlas(tex);

	if (texture.getAtlasId() != -1)
		return texture;

	int maxTexSize;
	glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxTexSize);
	int maxAtlasSize = min(1024, maxTexSize);

	int maxNeededAtlasSize = tex.size * 256;
	int atlasSize = min(maxNeededAtlasSize, maxAtlasSize);

	if (atlasSize < tex.size)
		return texture;

	unsigned int id;
	glGenTextures(1, &id);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, id);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, atlasSize, atlasSize, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
	
	TextureAtlas atlas(id, atlasSize, tex.size);
	textureAtlases.push_back(atlas);

	glBindTexture(GL_TEXTURE_2D, 0);

	return storeTextureInFreeAtlas(tex);
}

TextureAtlas ResourceManager::getAtlasInfo(int atlas)
{
	return textureAtlases[atlas];
}

Font ResourceManager::loadFont(string path)
{
	PNGTex texture = loadTextureFromFile(path, LCT_GREY);

	unsigned int id;
	glGenTextures(1, &id);

	glBindTexture(GL_TEXTURE_2D, id);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA8, texture.size, texture.size, 0, GL_ALPHA, GL_UNSIGNED_BYTE, &((*texture.data)[0]));

	Font font(id, texture.size);
	textureAtlases.push_back(font);

	glBindTexture(GL_TEXTURE_2D, 0);

	return font;
}