#pragma once

#include <common.h>

#include <Render\Resource\TextureAtlas.h>
#include <Render\Resource\Texture.h>
#include <Render\Resource\Font.h>

class ResourceManager
{
	struct PNGTex
	{
		unsigned int size;
		vector<unsigned char> * data;
	};

	ResourceManager(){}
	ResourceManager(const ResourceManager &);
	ResourceManager & operator=(ResourceManager &);

	vector<TextureAtlas> textureAtlases;
	vector<Texture> textures;

	PNGTex loadTextureFromFile(string path, int type = LCT_RGBA);
	Texture storeTextureInFreeAtlas(PNGTex texture);

public:

	static ResourceManager & getInstance();

	Texture loadTexture(string path);
	TextureAtlas getAtlasInfo(int atlas);

	Font loadFont(string path);
};