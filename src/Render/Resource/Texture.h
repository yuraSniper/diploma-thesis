#pragma once

#include <common.h>

#include <Render\Resource\TextureAtlas.h>

class Texture
{
	int textureAtlas, row, column;
	int id;
	float minU, minV;
	float maxU, maxV;

	TextureAtlas atlas;

public:
	Texture(TextureAtlas atlas, int textureAtlas = -1, int id = -1, int slot = 0);

	int getId();
	int getAtlasId();
	int getAtlasSlot();
	TextureAtlas getAtlasInfo();
	float getMinU();
	float getMaxU();
	float getMinV();
	float getMaxV();
};