#include "TextureAtlas.h"

TextureAtlas::TextureAtlas(unsigned int id, int size, int textureSize)
{
	this->id = id;
	this->size = size;
	this->textureSize = textureSize;
	this->freeSlots = getMaxSlots();
}

unsigned int TextureAtlas::getId()
{
	return id;
}

int TextureAtlas::getSize()
{
	return size;
}

int TextureAtlas::getTexureSize()
{
	return textureSize;
}

int TextureAtlas::getTexturesInRow()
{
	return size / textureSize;
}

int TextureAtlas::getMaxSlots()
{
	return getTexturesInRow() * getTexturesInRow();
}

void TextureAtlas::bind(int unit)
{
	glActiveTexture(GL_TEXTURE0 + unit);
	glBindTexture(GL_TEXTURE_2D, id);
}

int TextureAtlas::getFreeSlots()
{
	return freeSlots;
}

bool TextureAtlas::decrementFreeSlots()
{
	if (freeSlots == 0)
		return false;
	freeSlots--;
	return true;
}