#include "Texture.h"

Texture::Texture(TextureAtlas atlas, int textureAtlas, int id, int slot) : atlas(atlas)
{
	this->textureAtlas = textureAtlas;
	this->id = id;
	row = (slot / atlas.getTexturesInRow());
	column = slot % atlas.getTexturesInRow();

	minU = (float) column * atlas.getTexureSize() / (float) atlas.getSize();
	minV = (float) row * atlas.getTexureSize() / (float) atlas.getSize();
	maxU = (float) ((column + 1) * atlas.getTexureSize() - 1) / (float) atlas.getSize();
	maxV = (float) ((row + 1) * atlas.getTexureSize() - 1) / (float) atlas.getSize();
}

int Texture::getId()
{
	return id;
}

int Texture::getAtlasId()
{
	return textureAtlas;
}

int Texture::getAtlasSlot()
{
	return row * atlas.getTexturesInRow() + column;
}

TextureAtlas Texture::getAtlasInfo()
{
	return atlas;
}

float Texture::getMinU()
{
	return minU;
}

float Texture::getMaxU()
{
	return maxU;
}

float Texture::getMinV()
{
	return minV;
}

float Texture::getMaxV()
{
	return maxV;
}