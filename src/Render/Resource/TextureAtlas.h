#pragma once

#include <common.h>

class TextureAtlas
{
	unsigned int id;
	int size, textureSize;
	int freeSlots;

public:
	TextureAtlas(unsigned int id, int size, int textureSize);

	unsigned int getId();
	int getSize();
	int getTexureSize();
	int getMaxSlots();
	virtual int getTexturesInRow();
	virtual int getFreeSlots();
	virtual bool decrementFreeSlots();
	void bind(int unit);
};