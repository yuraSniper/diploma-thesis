#pragma once

#include <common.h>
#include <Render\Resource\TextureAtlas.h>

class Font : public TextureAtlas
{
public:
	Font(unsigned int id = -1, int size = 1000);

	virtual int getTexturesInRow();
	virtual int getFreeSlots();
	virtual bool decrementFreeSlots();

};