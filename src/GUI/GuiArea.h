#pragma once

#include <common.h>

class GuiArea
{
public:
	int x, y;
	int width, height;
	float padding;

	GuiArea(int x = 0, int y = 0, int width = 0, int height = 0, float padding = 0);

	void move(int x, int y);
	void resize(int width, int height);

	bool hitTest(POINT p);
	void setupGLViewport();
	void resetGLViewport();
};