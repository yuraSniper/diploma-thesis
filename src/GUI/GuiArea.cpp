#include "GuiArea.h"

#include <Client\Client.h>

#include <Window\Window.h>

GuiArea::GuiArea(int x, int y, int width, int height, float padding)
{
	this->x = x;
	this->y = y;
	this->width = width;
	this->height = height;
	this->padding = padding;
}

bool GuiArea::hitTest(POINT p)
{
	p.x -= this->x;
	p.y -= this->y;

	return (p.x >= 0 && p.x <= this->width) && (p.y >= 0 && p.y <= this->height);
}

void GuiArea::setupGLViewport()
{
	int wndHeight = Client::getInstance().mainWindow->getClientHeight();

	glPushAttrib(GL_SCISSOR_BIT | GL_VIEWPORT_BIT | GL_COLOR_BUFFER_BIT);
	glPushAttrib(GL_TRANSFORM_BIT);
	glViewport(x - padding, wndHeight - height - (y + padding), width + padding * 2, height + padding * 2);
	glScissor(x - padding, wndHeight - height - (y + padding), width + padding * 2, height + padding * 2);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(-padding, width + padding, height + padding, -padding, 0, 1);

	glPopAttrib();
}

void GuiArea::resetGLViewport()
{
	glPushAttrib(GL_TRANSFORM_BIT);
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glPopAttrib();
	glPopAttrib(); // GL_SCISSOR_BIT | GL_VIEWPORT_BIT | GL_COLOR_BUFFER_BIT
}

void GuiArea::move(int x, int y)
{
	this->x += x;
	this->y += y;
}

void GuiArea::resize(int width, int height)
{
	this->width += width;
	this->height += height;
}