#include "GUI.h"

GUI::GUI(shared_ptr<GUI> prev)
{
	this->prevGUI = prev;
}

shared_ptr<GUI> GUI::getPrevGUI()
{
	return prevGUI;
}