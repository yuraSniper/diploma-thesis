#pragma once

#include <common.h>

#include <GUI\GuiArea.h>

class Input;

class GUI
{
	shared_ptr<GUI> prevGUI;
public:
	GuiArea boundingBox;

	GUI(shared_ptr<GUI> prev);

	virtual void draw(POINT mousePos) = 0;
	virtual void checkInput(shared_ptr<Input> input) = 0;

	shared_ptr<GUI> getPrevGUI();
};