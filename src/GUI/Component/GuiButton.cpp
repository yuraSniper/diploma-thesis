#include "GuiButton.h"

#include <Render\TextRenderer.h>

#include <Window\Input.h>

GuiButton::GuiButton(string label, int x, int y, int width, int height)
{
	boundingBox = GuiArea(x, y, width, height, 1);
	this->label = label;
}

void GuiButton::draw(POINT mousePos)
{
	boundingBox.setupGLViewport();

	glClearColor(0.4, 0.4, 0.4, 1);
	glClear(GL_COLOR_BUFFER_BIT);
	
	if (isDown)
		glColor3f(0.15, 0.15, 0.15);
	else if (boundingBox.hitTest(mousePos))
		glColor3f(0.3, 0.3, 0.3);
	else
		glColor3f(0.25, 0.25, 0.25);
	
	glBegin(GL_TRIANGLE_STRIP);
	glVertex2f(0, 0);
	glVertex2f(0, boundingBox.height);
	glVertex2f(boundingBox.width, 0);	
	glVertex2f(boundingBox.width, boundingBox.height);
	glEnd();

	TextRenderer::getInstance().renderStringFormat(0, (boundingBox.height - TextRenderer::getInstance().height) / 2.0f,
		"%s", label.c_str());

	boundingBox.resetGLViewport();
}

bool GuiButton::checkInput(shared_ptr<Input> input)
{
	static bool last = false;
	bool current = input->getMouseButton(LEFT);

	if (!last && current)
		if (boundingBox.hitTest(input->getMousePos()))
			isDown = true;

	if (isDown && !input->getMouseButton(LEFT))
	{
		isDown = false;
		if (boundingBox.hitTest(input->getMousePos()))
			return true;
	}

	last = current;
	return false;
}