#pragma once

#include <common.h>

#include <GUI\GuiArea.h>

class Input;

class GuiButton
{
	string label;

public:
	GuiArea boundingBox;
	bool isDown = false;

	GuiButton(string label = "", int x = 0, int y = 0, int width = 0, int height = 0);

	void draw(POINT mousePos);
	bool checkInput(shared_ptr<Input> input);
};