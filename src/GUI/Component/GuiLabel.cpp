#include "GuiLabel.h"

#include <Render\TextRenderer.h>

GuiLabel::GuiLabel(string label, int x, int y, int width, int height)
{
	boundingBox = GuiArea(x, y, width, height);
	this->label = label;
}

void GuiLabel::draw()
{
	boundingBox.setupGLViewport();

	TextRenderer::getInstance().renderStringFormat(0, 0, "%s", label.c_str());

	boundingBox.resetGLViewport();
}