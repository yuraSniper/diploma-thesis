#pragma once

#include <common.h>

#include <GUI\GuiArea.h>

class Input;
class CircuitElement;

class GuiCanvas
{
	vector<shared_ptr<CircuitElement>> boxes;
	POINT prevMousePos;
	shared_ptr<CircuitElement> selected;
public:
	GuiArea boundingBox;

	GuiCanvas(int x = 0, int y = 0, int width = 0, int height = 0);

	void draw(POINT mousePos);
	void checkInput(shared_ptr<Input> input);
	void addBox();
};