#pragma once

#include <common.h>

#include <GUI\GuiArea.h>

class GuiLabel
{
	string label;
public:
	GuiArea boundingBox;

	GuiLabel(string label = "", int x = 0, int y = 0, int width = 0, int height = 0);

	void draw();
};