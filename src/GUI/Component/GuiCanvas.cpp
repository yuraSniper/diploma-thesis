#include "GuiCanvas.h"

#include <Client\Client.h>

#include <Circuit\CircuitElement.h>
#include <Circuit\GateAND.h>

#include <Window\Input.h>

GuiCanvas::GuiCanvas(int x, int y, int width, int height)
{
	boundingBox = GuiArea(x, y, width, height, 1);

	prevMousePos = {-1, -1};
}

void GuiCanvas::draw(POINT mousePos)
{
	boundingBox.setupGLViewport();

	mousePos.x -= boundingBox.x;
	mousePos.y -= boundingBox.y;

	glClearColor(0.15, 0.15, 0.15, 1);
	glClear(GL_COLOR_BUFFER_BIT);

	for (shared_ptr<CircuitElement> box : boxes)
	{
		GuiArea bb = box->boundingBox;
		if (box == selected)
		{
			glColor3f(0.7, 1, 0);
		}
		else if (bb.hitTest(mousePos))
		{
			glColor3f(1, 0.5, 0);
		}
		else
		{
			glColor3f(0.7, 0.35, 0);
		}

		box->draw();
	}

	boundingBox.resetGLViewport();
}

void GuiCanvas::checkInput(shared_ptr<Input> input)
{
	static bool last = false;
	bool current = input->getMouseButton(LEFT);

	POINT mousePos = input->getMousePos();
	if (boundingBox.hitTest(mousePos))
	{
		mousePos.x -= boundingBox.x;
		mousePos.y -= boundingBox.y;

		if (!last && current)
		{
			bool b = false;
			for (auto box = boxes.rbegin(); box != boxes.rend(); box++)
			{
				if ((*box)->boundingBox.hitTest(mousePos))
				{
					selected = *box;
					prevMousePos = mousePos;
					b = true;
					break;
				}
			}

			if (!b)
			{
				selected = nullptr;
				prevMousePos = {-1, -1};
			}
		}

		if (selected != nullptr && input->getMouseButton(LEFT))
		{
			if (prevMousePos.x != -1 && prevMousePos.y != -1)
			{
				selected->boundingBox.move(mousePos.x - prevMousePos.x, mousePos.y - prevMousePos.y);
				prevMousePos = mousePos;
			}
		}
	}

	if (!current)
		prevMousePos = {-1, -1};

	last = current;
}

void GuiCanvas::addBox()
{
	boxes.push_back(CircuitElement::make<GateAND>(GuiArea(100, 100, 48, 48)));
}