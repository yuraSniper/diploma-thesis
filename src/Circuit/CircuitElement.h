#pragma once

#include <common.h>

#include <GUI\GuiArea.h>

class CircuitElement
{
	unsigned int displayList;
protected:
	CircuitElement();
public:
	GuiArea boundingBox;

	void draw();
	virtual void initDraw() = 0;

	template<class T>
	static shared_ptr<CircuitElement> make(GuiArea bb)
	{
		CircuitElement * e = new T(T::standard);
		shared_ptr<CircuitElement> el = shared_ptr<CircuitElement>(e);
		el->boundingBox = bb;

		if (T::standard.displayList == -1)
		{
			el->displayList = T::standard.displayList = glGenLists(1);
			glNewList(el->displayList, GL_COMPILE);

			el->initDraw();

			glEndList();
		}

		return el;
	}
};