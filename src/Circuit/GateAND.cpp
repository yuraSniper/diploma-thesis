#include "GateAND.h"

GateAND GateAND::standard;

void GateAND::initDraw()
{
	glBegin(GL_POLYGON);
	glVertex2f(0, 0);
	glVertex2f(0, 1);
	glVertex2f(0.5, 1);

	for (float a = -M_PI_2; a <= M_PI_2; a += M_PI / 16)
		glVertex2f(cos(a) * 0.5f + 0.5f, 1 - (sin(a) + 1.0f) * 0.5f);

	glEnd();
}