#pragma once

#include <common.h>
#include <Circuit\CircuitElement.h>

class GateAND : public CircuitElement
{
	friend class CircuitElement;
	GateAND() {}
	static GateAND standard;
public:	

	virtual void initDraw();
	bool eval();
};