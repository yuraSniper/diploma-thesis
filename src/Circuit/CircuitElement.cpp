#include "CircuitElement.h"

#include <Render\Resource\Texture.h>
#include <Render\Resource\ResourceManager.h>

CircuitElement::CircuitElement()
{
	displayList = -1;
}

void CircuitElement::draw()
{
	glPushMatrix();
	glTranslatef(boundingBox.x, boundingBox.y, 0);
	glScalef(boundingBox.width, boundingBox.height, 1);

	glCallList(displayList);

	glPopMatrix();
}