#include "Client.h"

#include <Client\Thread\RenderThread.h>

#include <Client\GUI\TmpGUI.h>

#include <Window\Input.h>
#include <Window\Window.h>

Client & Client::getInstance()
{
	static Client instance;
	return instance;
}

void Client::onClose(void * param, void *)
{
	((Client *)param)->working = false;
}

Client::Client()
{
	mainWindow = make_shared<Window>(800, 600);
	mainWindow->onCLose.add(onClose, this);

	renderThread = make_shared<RenderThread>();

	input = make_shared<Input>(mainWindow);
	mainWindow->setVisibility(true);
	mainLoop();
}

void Client::mainLoop()
{
	currentGui = make_shared<TmpGUI>(nullptr);
	renderThread->resume();
	while (working)
	{
		mainWindow->messageLoop();
	}
}

void Client::initGL()
{
	rc = wglCreateContext(mainWindow->getHDC());
	wglMakeCurrent(mainWindow->getHDC(), rc);

	cout << "GL_VERSION : " << glGetString(GL_VERSION) << endl;

	glewInit();
	if (__WGLEW_EXT_swap_control)
		wglSwapIntervalEXT(0);
	else
		cout << "wglSwapIntervalEXT is not supported." << endl;

	glClearColor(0.1, 0.1, 0.1, 1);

	glEnable(GL_SCISSOR_TEST);
}

void Client::drawFrame()
{
	int width = mainWindow->getClientWidth();
	int height = mainWindow->getClientHeight();

	glScissor(0, 0, width, height);
	glViewport(0, 0, width, height);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	currentGui->boundingBox = GuiArea(0, 0, width, height);
	currentGui->boundingBox.setupGLViewport();
	currentGui->draw(input->getMousePos());
	currentGui->boundingBox.resetGLViewport();

	currentGui->checkInput(input);

	mainWindow->swapBuffers();
}