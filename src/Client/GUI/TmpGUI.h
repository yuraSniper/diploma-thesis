#pragma once

#include <common.h>
#include <GUI\GUI.h>

class GuiButton;
class GuiLabel;

class TmpGUI : public GUI
{
	shared_ptr<GuiButton> button;
	shared_ptr<GuiLabel> label;
public:
	TmpGUI(shared_ptr<GUI> gui);

	virtual void draw(POINT mousePos);
	virtual void checkInput(shared_ptr<Input> input);
};