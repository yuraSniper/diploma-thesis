#include "TmpGUI.h"

#include <GUI\Component\GuiButton.h>
#include <GUI\Component\GuiLabel.h>

#include <Window\Input.h>

TmpGUI::TmpGUI(shared_ptr<GUI> gui) : GUI(gui)
{
	button = make_shared<GuiButton>(100, 100, 125, 25, "Test button");
	label = make_shared<GuiLabel>(50, 50, 125, 25, "Test label");
}

void TmpGUI::draw(POINT mousePos)
{
	button->draw(mousePos);
	label->draw();
}

void TmpGUI::checkInput(shared_ptr<Input> input)
{
	if (button->boundingBox.hitTest(input->getMousePos()) && input->getMousePressed(0))
		MessageBoxA(nullptr, "Button pressed", "Test button", MB_OK);
}