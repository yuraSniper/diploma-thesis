#pragma once

#include <common.h>

class Window;
class Input;
class Thread;
class GUI;

class Client
{
	Client();
	Client(Client &);
	void operator=(Client &);

	shared_ptr<Thread> renderThread;
	shared_ptr<GUI> currentGui;

	HGLRC rc;

	bool working = true;

	static void onClose(void * param, void *);
	void mainLoop();

public:
	shared_ptr<Window> mainWindow;
	shared_ptr<Input> input;
	
	static Client & getInstance();

	void initGL();
	void drawFrame();
};