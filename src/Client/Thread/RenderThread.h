#pragma once

#include <common.h>
#include <Thread\Thread.h>
#include <Client\Client.h>

class RenderThread : public Thread
{
public:
	void run()
	{
		Client & client = Client::getInstance();
		client.initGL();

		while (true)
		{
			client.drawFrame();
		}
	}
};