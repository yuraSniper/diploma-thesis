#pragma once

#include <Windows.h>
#include <windowsx.h>

#define _USE_MATH_DEFINES
#include <cmath>

#include <algorithm>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <unordered_map>
#include <memory>
#include <queue>
#include <set>

#include <Lib\Glew\glew.h>
#include <Lib\Glew\wglew.h>
#include <Lib\LodePNG\lodepng.h>

#include <Util\Profiling\Timer.h>

using namespace std;